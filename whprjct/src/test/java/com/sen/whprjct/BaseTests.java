
package com.sen.whprjct;

import com.sen.whprjct.pages.TestInsCmpnyProfilePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseTests {
    private final String driverKey = "webdriver.gecko.driver";
    private final String driverPath = "resources/geckodriver.exe";
    private final String testInsCompanyUrl = "http://wallethub.com/profile/test_insurance_company/";
    
    protected WebDriver driver;
    
    protected TestInsCmpnyProfilePage insuranceCompanyProfilePage;
    
    @BeforeClass
    public void setup() {
        System.setProperty(driverKey, driverPath);
        driver = new FirefoxDriver();
    }

    public void launchTestInsCmpnProfileSite() {
        driver.get(testInsCompanyUrl);
//        driver.manage().window().maximize();
        insuranceCompanyProfilePage = new TestInsCmpnyProfilePage(driver);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
