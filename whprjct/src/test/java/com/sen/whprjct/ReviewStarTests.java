
package com.sen.whprjct;

import com.sen.whprjct.pages.LoginPage;
import com.sen.whprjct.pages.UserProfilePage;

import static org.testng.AssertJUnit.assertTrue;
import org.testng.annotations.Test;

public class ReviewStarTests extends BaseTests {
    
    @Test
    public void testReivewSubmission() throws Exception {
        int nthStar = 4;
        String reviewText = "SMxNVL10FV1ey0R8c2tayxCSAzah8fDgHlznFaaaW39YV7mfE5TYGcmdx0aWsznvR0zRNlyLbV8OcYYpwzBNCULZB8qcJfFwrDalInG9s2fXKDdKWafQUjMxcjCOghiM7XZXfAhN7fy2Qbt3UmhfxcScGqWiqslUTsIWdpI0b5lf8xUKw4WvdVo1VG33ulZBtzkDs8Sn4wwiHM";
        String policy = "Health Insurance";
        String whUsername = "whtestsm@hotmail.com";
        String whPassword = "whChang3!t";
        String reviewedCompanyName = "Test Insurance Company";
        String userName = "whtestsm";
        
        launchTestInsCmpnProfileSite(); // step 1
        
        assertTrue(this.insuranceCompanyProfilePage.hoverOverThrough(nthStar)); // step 2a and 2b
        
        this.insuranceCompanyProfilePage.clickOnCurrentStar(); // step 2c
        
        this.insuranceCompanyProfilePage.selectPolicy(policy); // step 3
        
        LoginPage loginPage = this.insuranceCompanyProfilePage.writeReview(reviewText); // step 4 and 5
        
        loginPage.login(whUsername, whPassword); // logging in completes the submission
        assertTrue(this.insuranceCompanyProfilePage.isReviewTextUpdated(reviewText)); // step 6 - confirmation not displayed per instructions, so just checking for the review text update in the feed
        
        UserProfilePage userProfilePage = new UserProfilePage(driver);
        assertTrue(userProfilePage.isReiviewVisibleinUserProfilePage(userName, reviewedCompanyName)); // step 7 - review text not displayed per instructions, so just checking for reviewed company name
        
        /**
        * Please note that the application under test does not behave as 
        * described in assignment 2 - step 6, i.e., confirmation screen not 
        * shown, but getting redirected to the review feed, so just asserting 
        * the text is updated. Also in step 7, the review text is not displayed 
        * but just the reviewed company name, per recruiter's (Radhika Singhal) 
        * suggestion, asserting only if the company name is displayed.
        */ 
    }
}
