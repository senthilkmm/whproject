
package com.sen.whprjct.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private final WebDriver driver;
    
    private final By loginTabLink = By.xpath("//a[contains(@ng-click,'login')]");
    private final By emailAddress = By.xpath("//input[@name='em']");
    private final By password = By.name("pw1");
    
    private WebDriverWait wait;
    private final long timeOutInSeconds = 20;
    
    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * login to complete the review submission
     * @param whUsername
     * @param whPassword
     */
    public void login(String whUsername, String whPassword) {
        wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.elementToBeClickable(loginTabLink)).click();
        this.driver.findElement(emailAddress).sendKeys(whUsername);
        this.driver.findElement(password).sendKeys(whPassword, Keys.ENTER);
    }
}
