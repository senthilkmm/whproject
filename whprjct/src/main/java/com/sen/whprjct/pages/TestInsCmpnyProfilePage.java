
package com.sen.whprjct.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestInsCmpnyProfilePage {
    private final WebDriver driver;
    private final Actions builder;
    
    private final By starElement = By.xpath("//*[@class='rvs-star-svg' and @width='38']");
    private final By dropDownPlaceHolder = By.xpath("//span[@class='dropdown-placeholder' and text()='Select...']");
    private final By writeReviewTextArea = By.xpath("//textarea[@placeholder='Write your review...']");
    private final By submitButton = By.xpath("//div[contains(@class,'sbn-action')]");
    
    private String policyItem = "//li[@class=\'dropdown-item\' and text()=\'%s\']";
//    private String reviewTextEntry = "//*[contains(@class,\'rvtab-ci-content\') and contains(text(),\'%s\')]";
    private String reviewTextEntry = "//*[contains(@class,\'rvtab-ci-content\')]";
    
    private WebDriverWait wait;
    private final long timeOutInSeconds = 20;
    
    public TestInsCmpnyProfilePage(WebDriver driver) {
        this.driver = driver;
        this.builder = new Actions(driver);
    }
    
    /**
     * hover over through the nth star
     * @param nthStar
     * @return if the stars are lit as hovered over
     * @throws Exception
     */
    public boolean hoverOverThrough(int nthStar) throws Exception {
        List<WebElement> ratingStars = driver.findElements(starElement);
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].scrollIntoView();", ratingStars.get(0));
        for (int i=0; i<nthStar; i++) {
            if (!hoverStar(ratingStars.get(i))) return false;
        }
        return true; // all the stars are hovered as expected
    }
    
    private boolean hoverStar(WebElement ratingStar) {
        Action hoverOver = this.builder.moveToElement(ratingStar).build();
        hoverOver.perform();
        return isStarLit(ratingStar); // ensure the star is lit
    }

    private boolean isStarLit(WebElement ratingStar) {
        int numberOfPathElements = ratingStar.findElement(By.tagName("g")).findElements(By.tagName("path")).size();
        return numberOfPathElements > 1; // highlighted star has 2 path elements
    }
    
    /**
     * click on the current star that is hovered
     * @throws Exception
     */
    public void clickOnCurrentStar() throws Exception {
        this.builder.click().build().perform();
    }
    
    /**
     * select a policy in the drop down
     * @param policy
     */
    public void selectPolicy(String policy) {
        WebElement policiesDropDown = driver.findElement(dropDownPlaceHolder);
        policyItem = String.format(policyItem, policy);
        WebElement policyOption = policiesDropDown.findElement(By.xpath(policyItem));
        this.builder.moveToElement(policiesDropDown).click().moveToElement(policyOption).click().build().perform();
    }
    
    /**
     * write review and submit
     * @param reviewText
     * @return LoginPage
     */
    public LoginPage writeReview(String reviewText) {
        driver.findElement(writeReviewTextArea).sendKeys(reviewText);
        driver.findElement(submitButton).click();
        // clicking on submit redirects to login page, hence returning the login page object to the caller
        return new LoginPage(driver);
    }
    
    /**
     * check if the review text updated in the feed
     * @param reviewText
     * @return if the review text is updated in the feed
     */
    public boolean isReviewTextUpdated(String reviewText) {
        reviewTextEntry = String.format(reviewTextEntry, reviewText);
        wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(reviewTextEntry)));
        return true;
    }
}
