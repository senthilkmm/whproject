
package com.sen.whprjct.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UserProfilePage {
    private final WebDriver driver;
    
    private String userProfileUrl = "https://wallethub.com/profile/%s";
    
    private By reviewedCompanyLink;
    
    public UserProfilePage(WebDriver driver) {
        this.driver = driver;
    }
    
    public void launchUserProfilePage(String userName) {
        userProfileUrl = String.format(userProfileUrl, userName);
        driver.get(userProfileUrl);
    }
    
    /**
     * check the user profile page to see if the reviewed company name is visible
     * @param userName
     * @param reviewedCompanyName
     * @return if the reviewed company name is visible
     */
    public boolean isReiviewVisibleinUserProfilePage(String userName, String reviewedCompanyName) {
        launchUserProfilePage(userName);
        reviewedCompanyLink = By.linkText(reviewedCompanyName);
        return this.driver.findElement(reviewedCompanyLink).isDisplayed();
    }
}
